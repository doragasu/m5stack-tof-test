# Introduction

This project is a template with the components required to support the M5Stack boards:

* The target platform is the M5Stack Fire.
* The supported esp-idf version is v4.4.

# Components

The following components are currently supported

## Screen

320x240 screen is supported via the [Light and Versatile Graphics Library (LVGL)](https://github.com/lvgl/lvgl/). Make sure the M5Stack driver is selected in `Component config --> LVGL ESP Drivers --> LVGL TFT Display controller`. The proper screen orientation is set in this section. Also ensure the screen dimensions are correct in `Component config --> LVGL configuration`, and that they match the orientation set earlier (it should be 240x320 for portrait or 320x240 for landscape orientations.

Note that in addition to the SPI interface, the LCD uses the following GPIO pins (that are thus not available for other applications):

* GPIO 14: CS
* GPIO 27: RS
* GPIO 32: BL
* GPIO 33: RST

## Backlight

LVGL ESP32 driver includes a PWM backlight driver, but when enabled in menuconfig, the driver structure will be allocated, backlight will be set to 100% and that's all, the backlight config structure reference is then lost and you cannot do anything with it. So if you want flexible PWM backlight control, the best thing you can do is disabling it in menuconfig (set `Component config --> LVGL ESP Drivers --> LVGL TFT Display controller --> Backlight Control` to `Not Used`), and then manually use the backlight driver as in the example code.

## Flash

The 16 MiB flash is directly supported by IDF. Make sure 16 MiB is selected in `Serial flasher config --> Flash size`.

## PSRAM

The 4 MiB SPI PSRAM is directly supported by IDF. Make sure it is enabled in `Component config --> ESP32-specific --> Support for external, SPI-connected RAM`.

## Pushbuttons

Pushbuttons A, B and C are directly connected to GPIO pins 39, 38 and 37. Thus they are directly supported using the IDF GPIO functions. Example code uses button A to control the screen backlight in 10% steps.

## Dual LED bar

The dual LED bar is directly supported by IDF using the RMT driver (`driver/rmt.h`), see the `led_strip` example included in IDF. Just make sure the GPIO 15 is used to control the LEDs, and that he number of LEDs is set to 10. The example code shows how to drive the leds using the `led_strip` component included in `esp-idf` examples. If you want to use this component make sure you add to your project `CMakeLists.txt` the following line:

```
set(EXTRA_COMPONENT_DIRS $ENV{IDF_PATH}/examples/common_components/led_strip)
```

## 6-axis IMU MPU6886

This IMU is connected to the I2C port, address 0x68. It is supported using the [esp\_mpu6886 driver](https://gitlab.com/doragasu/esp_mpu6886). For the driver to work, make sure to set SCL pin to GPIO 21 and SDA pin to GPIO 22 in `Component config --> MPU6886 configuration`.

## Magnetometer BMM150

This 3-axis digital geomagnetic sensor is connected to the I2C port, address 0x10. It is supported using the [ESP BMM150 driver](https://gitlab.com/doragasu/esp_bmm150), that uses the [Bosch Sensortec BMM150 Sensor API](https://github.com/BoschSensortec/BMM150-Sensor-API) under the hood.

## Speaker

The speaker is driven by the DAC using the GPIO 25. The sample code uses the cosine wave generator API to beep it twice.

## Microphone

The microphone is connected to the ADC using GPIO 34. You can browse the esp-idf ADC examples to learn about how to use it. 

## Battery readings

M5Stack Core modules manufactured after march 2018 manage battery charging using an IP5306 variant connected to the I2C bus. Unfortunately there is no complete documentation of this chip as of today (only a partial datasheet in Chinese, for more information read [here](https://community.m5stack.com/topic/878/solved-can-t-find-ip5306-i2c-address)). On boards with this chip, it is mapped at bus address 0x75, and battery level can be obtained by reading register 0x78. If your board supports this charge IC, the demo application shows the battery level (in 25% steps: 25, 50, 75 or 100) in the center of the spinner widget. If the chip is not found/supported, a `?` character will be shown instead.

## microSD card

MicroSD card slot is connected to the SPI bus, and uses GPIO 4 to drive the CS. Note that MOSI (GPIO23), MISO (GPIO19) and CLK (GPIO18) are shared with the screen controller. This causes the microSD card support to be a bit hacky. As the screen driver always tries to init the SPI bus, on a first approach we could just make sure the microSD card driver skips the SPI bus initialization. The problem is that due to unknown reasons (maybe an SPI bug?), if the screen is initialized before the microSD card, the card initialization will fail. Thus the only way I found so far to make the SD card work is to make the microSD card code initialize the card first (including the SPI bus) and then initialize the screen, but editing the screen initialization driver to skip the SPI bus setup. So if you need SD card, you must do the following:

1. Edit `components/lvgl_esp32_drivers/lvgl_helpers.c`. In the function `lvgl_driver_init()`, just after the `#if defined CONFIG_LV_TFT_DISPLAY_PROTOCOL_SPI`, comment out the following function call:

```C
    lvgl_spi_driver_init(TFT_SPI_HOST,
        DISP_SPI_MISO, DISP_SPI_MOSI, DISP_SPI_CLK,
        SPI_BUS_MAX_TRANSFER_SZ, 1,
        DISP_SPI_IO2, DISP_SPI_IO3);
```

2. Launch `menuconfig` and in `Component config --> LVGL ESP Drivers --> LVGL TFT Display controller` check `Use custom SPI clock frequency` and set it to **26.67 MHz**.
3. In your code, make sure you initialize the micro SD card first, then initialize the screen.

In addition to being a bit hacky, this affects display performance (because bus speed must be lowered from 40 MHz to 26.67 MHz), so it is not recommended using the microSD card support routines if the card is not needed. Due to these reasons, the SD card code is not included in the example code (in `main/main.c`), but if you want to use the SD card, you can find the code in the `sdcard` branch of this repository, just check it out.
